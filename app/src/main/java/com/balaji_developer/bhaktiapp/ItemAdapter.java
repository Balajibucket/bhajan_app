package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Random;



public class ItemAdapter extends BaseAdapter {
    ArrayList<Item> arrayListItem;
    private ArrayList<Item> arrayListItemCopy;
    private Context context;
    private Random mRandom = new Random();


    public ItemAdapter(Context context, ArrayList<Item> arrayListItem) {
        this.arrayListItem = new ArrayList();
        this.arrayListItem = new ArrayList(arrayListItem);
        this.arrayListItemCopy = new ArrayList(arrayListItem);
        this.context = context;
    }

    public int getCount() {
        return this.arrayListItem.size();
    }

    public Object getItem(int position) {
        return this.arrayListItem.get(position);
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.single_row_list, null);
        }
        TextView textViewTitleHindi = (TextView) convertView.findViewById(R.id.textViewTitleHindi);
        RelativeLayout outer_rel_lout =(RelativeLayout)convertView.findViewById(R.id.outer_rel_lout);
//        outer_rel_lout.setBackgroundColor(getRandomHSVColor());
        Item item = (Item) this.arrayListItem.get(position);
//        ((TextView) convertView.findViewById(R.id.textViewSNo)).setText(String.valueOf(position + 1));

        textViewTitleHindi.setText(item.getsTitle());

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Bold.ttf");
        textViewTitleHindi.setTypeface(font);

        ((ImageView) convertView.findViewById(R.id.textViewSNo)).setBackgroundColor(getRandomHSVColor());
        return convertView;
    }


    // Custom method to get a random number between a range
    protected int getRandomIntInRange(int max, int min){
        return mRandom.nextInt((max-min)+min)+min;
    }

    // Custom method to generate random HSV color
    protected int getRandomHSVColor(){
        // Generate a random hue value between 0 to 360
        int hue = mRandom.nextInt(361);
        // We make the color depth full
        float saturation = 1.0f;
        // We make a full bright color
        float value = 1.0f;
        // We avoid color transparency
        int alpha = 255;
        // Finally, generate the color
        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
        // Return the color
        return color;
    }



}
