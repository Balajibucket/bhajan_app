package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerViewHolder> {


    private final TypedArray menu_main_Icon;
    private final String[] menu_main_Item;
    Context context;
    LayoutInflater inflater;

    public RecyclerAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.menu_main_Item = context.getResources().getStringArray(R.array.bhajan_menu);
        this.menu_main_Icon = context.getResources().obtainTypedArray(R.array.bhajan_menu_icon);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = inflater.inflate(R.layout.item_list, parent, false);

        RecyclerViewHolder viewHolder = new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {

        holder.tv1.setText(menu_main_Item[position]);
        Glide.with(context).load(menu_main_Icon.getResourceId(position, -1))
                .into(holder.imageView);

        holder.imageView.setOnClickListener(clickListener);
        holder.imageView.setTag(holder);
    }

    View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerViewHolder vholder = (RecyclerViewHolder) v.getTag();
            int position = vholder.getPosition();

//            Toast.makeText(context, "This is position " + position, Toast.LENGTH_LONG).show();

            Intent intentTitle = new Intent(context, TitleActivity.class);
            intentTitle.putExtra("god", Constant.bhajan[position]);
            intentTitle.putExtra("actionBarTitle", menu_main_Item[position]);

            context.startActivity(intentTitle);

        }
    };


    @Override
    public int getItemCount() {
        return menu_main_Item.length;
    }


    /****************************************************************/
    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        TextView tv1, tv2;
        CircleImageView imageView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);

            tv1 = (TextView) itemView.findViewById(R.id.list_title);
            tv2 = (TextView) itemView.findViewById(R.id.list_desc);
            imageView = (CircleImageView) itemView.findViewById(R.id.list_avatar);

        }
    }


}
