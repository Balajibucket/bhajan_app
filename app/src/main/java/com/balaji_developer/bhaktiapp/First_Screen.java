package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.ExploreByTouchHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;


public class First_Screen extends AppCompatActivity {


    private CardView card1;
    private CardView card2;
    private CardView card3;
    private CardView card4;
    private CardView card5;
    private CardView card6,card7;
    private InterstitialAd mInterstitialAd;

    private AlertDialog dialog;
    private AdRequest adRequest;
    private AdView adView;
    private Context context;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.firstscreen1);
//        if (Build.VERSION.SDK_INT >= 21) {
//            getWindow().addFlags(ExploreByTouchHelper.INVALID_ID);
//            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
//        }


        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(getResources().getString(R.string.titleApp));
        this.context = getApplicationContext();
        getSupportActionBar().setIcon(ContextCompat.getDrawable(this.context, R.mipmap.ic_launcher));


        this.card1 = (CardView) findViewById(R.id.card_view1);
        this.card2 = (CardView) findViewById(R.id.card_view2);
        this.card3 = (CardView) findViewById(R.id.card_view3);
        this.card4 = (CardView) findViewById(R.id.card_view4);
        this.card5 = (CardView) findViewById(R.id.card_view5);
        this.card6 = (CardView) findViewById(R.id.card_view6);
        this.card7 = (CardView) findViewById(R.id.card_view7);
//        initAdmobCode();


        adView = (AdView) findViewById(R.id.adView);
        adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.minterstitialad_ad_unit_id_main));

        mInterstitialAd.loadAd(adRequest);
        

        this.card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayInterstitial();

                Intent intentTitle = new Intent(First_Screen.this, HomeActivity.class);
                startActivity(intentTitle);

            }
        });
        this.card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayInterstitial();

                Intent intentTitle = new Intent(First_Screen.this, TitleActivity.class);
                intentTitle.putExtra("god", "chalisa");
                intentTitle.putExtra("actionBarTitle", "चालीसा");

                startActivity(intentTitle);

            }
        });
        this.card4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                displayInterstitial();
                Intent intentTitle = new Intent(First_Screen.this, TitleActivity.class);
                intentTitle.putExtra("god", "mantra");
                intentTitle.putExtra("actionBarTitle", "मंत्र");

                startActivity(intentTitle);

            }
        });
        this.card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                displayInterstitial();

                Intent intentTitle = new Intent(First_Screen.this, TitleActivity.class);
                intentTitle.putExtra("god", "aarti");
                intentTitle.putExtra("actionBarTitle", "आरती");

                startActivity(intentTitle);



            }
        });

        this.card7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                displayInterstitial();
                String msg = getResources().getString(R.string.share_msg);
                Intent intent2 = new Intent();
                intent2.setAction("android.intent.action.SEND");
                intent2.setType("text/plain");
                intent2.putExtra("android.intent.extra.TEXT", msg);
                startActivity(Intent.createChooser(intent2, "Share via"));



            }
        });


        this.card5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(First_Screen.this);
                builder.setMessage(First_Screen.this.getResources().getString(R.string.otherapp_msg));
                builder.setTitle(First_Screen.this.getResources().getString(R.string.otherapp_title));
                builder.setPositiveButton(First_Screen.this.getResources().getString(R.string.otherrate_it), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        displayInterstitial();
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://play.google.com/store/apps/developer?id=Digital_Learning&hl=en")));


                    }
                });

                builder.setNegativeButton(First_Screen.this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                First_Screen.this.dialog = builder.create();
                First_Screen.this.dialog.show();

            }
        });
        this.card6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(First_Screen.this);
                builder.setMessage(First_Screen.this.getResources().getString(R.string.ratethisapp_msg));
                builder.setTitle(First_Screen.this.getResources().getString(R.string.ratethisapp_title));
                builder.setPositiveButton(First_Screen.this.getResources().getString(R.string.otherrate_it), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
//                        displayInterstitial();
                        startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + First_Screen.this.getPackageName())));



                    }
                });
                builder.setNegativeButton(First_Screen.this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                First_Screen.this.dialog = builder.create();
                First_Screen.this.dialog.show();


            }
        });


    }


    private boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(First_Screen.this.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void displayInterstitial() {
        if (this.mInterstitialAd.isLoaded()) {
            this.mInterstitialAd.show();
        }
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_open_favourite) {
            Intent intent = new Intent(getApplicationContext(), TitleActivity.class);
            intent.putExtra("god", "favourite");
            intent.putExtra("actionBarTitle", "\u092e\u0947\u0930\u0947 \u092a\u0938\u0902\u0926\u0940\u0926\u093e (Favourite)");
            startActivity(intent);
            return true;
        } else if (id == R.id.action_share_app) {
            String msg = getResources().getString(R.string.share_msg);
            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.SEND");
            intent2.setType("text/plain");
            intent2.putExtra("android.intent.extra.TEXT", msg);
            startActivity(Intent.createChooser(intent2, "Share via"));
            return true;
        } else {
            if (id == R.id.action_rate_it) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + First_Screen.this.getPackageName())));
            }

            else if (id == R.id.action_our_apps) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Digital_Learning")));
            }

            else if (id == R.id.action_disclaimer) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
                builder.setTitle((CharSequence) "Disclaimer");
                builder.setMessage(getResources().getString(R.string.discl));
                builder.setPositiveButton((CharSequence) "Ok", null);
                builder.setIcon((int) R.mipmap.ic_launcher);
                builder.show();
//                if (this.mInterstitialAd.isLoaded()) {
//                    this.mInterstitialAd.show();
//                } else {
//                    requestNewInterstitial();
//                }
            }
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

//        initAdmobCode();

        if (Utilities.checkInternet(this)) {
            adView.setVisibility(View.VISIBLE);
            adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
            adView.loadAd(adRequest);
            return;
        } else {
            adView.setVisibility(View.GONE);
            return;
        }

    }


    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }



    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }

}
