package com.balaji_developer.bhaktiapp;

import java.io.Serializable;

public class Item implements Serializable {
    boolean bIsFavourite;
    int id;
    String sDescription;
    String sTitle;
    String sType;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getsType() {
        return this.sType;
    }

    public void setsType(String sType) {
        this.sType = sType;
    }

    public String getsTitle() {
        return this.sTitle;
    }

    public void setsTitle(String sTitle) {
        this.sTitle = sTitle;
    }

    public String getsDescription() {
        return this.sDescription;
    }

    public void setsDescription(String sDescription) {
        this.sDescription = sDescription;
    }

    public boolean getbIsFavourite() {
        return this.bIsFavourite;
    }

    public void setbIsFavourite(boolean bIsFavourite) {
        this.bIsFavourite = bIsFavourite;
    }

    public String getString() {
        return this.sTitle + this.sType;
    }
}
