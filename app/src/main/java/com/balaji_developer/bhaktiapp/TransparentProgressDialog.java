package com.balaji_developer.bhaktiapp;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;


public class TransparentProgressDialog extends Dialog {

	ProgressBar progressBar;

	public TransparentProgressDialog(Context context) {
		super(context, R.style.TransparentProgressDialog);
		WindowManager.LayoutParams wlmp = getWindow().getAttributes();
		wlmp.gravity = Gravity.CENTER_HORIZONTAL;
		getWindow().setAttributes(wlmp);
		setTitle(null);
		setCancelable(false);
		setOnCancelListener(null);
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT);
		// iv = new ImageView(context);
		// iv.setImageResource(R.drawable.loader_large_blue);
		// layout.addView(iv, params);

		progressBar = new ProgressBar(context, null, android.R.attr.progressBarStyleLarge);
		progressBar.setIndeterminate(true);
//		progressBar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
		
		layout.addView(progressBar, params);
		addContentView(layout, params);
	}

	// private ImageView iv;
	//
	// public TransparentProgressDialog(Context context) {
	// super(context, R.style.TransparentProgressDialog);
	// WindowManager.LayoutParams wlmp = getWindow().getAttributes();
	// wlmp.gravity = Gravity.CENTER_HORIZONTAL;
	// getWindow().setAttributes(wlmp);
	// setTitle(null);
	// setCancelable(false);
	// setOnCancelListener(null);
	// LinearLayout layout = new LinearLayout(context);
	// layout.setOrientation(LinearLayout.VERTICAL);
	// LinearLayout.LayoutParams params = new
	// LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
	// LayoutParams.WRAP_CONTENT);
	// iv = new ImageView(context);
	// iv.setImageResource(R.drawable.loader_large_blue);
	// layout.addView(iv, params);
	// addContentView(layout, params);
	// }
	//
	// @Override
	// public void show() {
	// super.show();
	// RotateAnimation anim = new RotateAnimation(0.0f, 360.0f,
	// Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
	// anim.setInterpolator(new LinearInterpolator());
	// anim.setRepeatCount(Animation.INFINITE);
	// anim.setDuration(400);
	// iv.setAnimation(anim);
	// iv.startAnimation(anim);
	// }
}