package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;


public class TitleActivity extends AppCompatActivity {
    private String actionBarTitle;
    ItemAdapter adapter;
    private Context context;
//    int flag;
    String god;
    DataBaseHelper helper;
    Intent intentTitle;
    private ListView listView;
//    private InterstitialAd mInterstitialAd;

    public static InterstitialAd mInterstitialAd;
    private AdView adView;
    private AdRequest adRequest;

//    public TitleActivity() {
//        this.flag = -1;
//    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_title);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.context = getApplicationContext();
        this.listView = (ListView) findViewById(R.id.listView);


        adView = (AdView) findViewById(R.id.adView);
        adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.minterstitialad_ad_unit_id_main));

        mInterstitialAd.loadAd(adRequest);


        this.helper = new DataBaseHelper(this.context);
        if (getIntent().getExtras() != null) {
            this.actionBarTitle = getIntent().getExtras().getString("actionBarTitle");
            this.god = getIntent().getExtras().getString("god");
            getSupportActionBar().setTitle(this.actionBarTitle);
            this.adapter = new ItemAdapter(this.context, this.helper.getItemsList(this.god));
            this.listView.setAdapter(this.adapter);
            this.listView.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Item item = (Item) parent.getAdapter().getItem(position);
                    TitleActivity.this.intentTitle = new Intent(TitleActivity.this.context, DescriptionActivity.class);
                    TitleActivity.this.intentTitle.putExtra("item", item);
                    if (position % 2 == 0 && TitleActivity.this.mInterstitialAd.isLoaded()) {
//                        TitleActivity.this.mInterstitialAd.show();
//                        TitleActivity.this.flag = 3;
//                        return;

                        showInterstitial();
                    }
//                    TitleActivity.this.requestNewInterstitial();
//                    TitleActivity.this.flag = -1;
                    TitleActivity.this.startActivity(TitleActivity.this.intentTitle);
                }
            });
        }
//        initAdmobCode();
    }

//    public void initAdmobCode() {
//        MobileAds.initialize(getApplicationContext(), this.context.getString(R.string.app_id));
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        if (isNetworkAvailable()) {
//            mAdView.setVisibility(View.VISIBLE);
//            mAdView.loadAd(new AdRequest.Builder().build());
//        } else {
//            mAdView.setVisibility(View.GONE);
//        }
//        this.mInterstitialAd = new InterstitialAd(this);
//        this.mInterstitialAd.setAdUnitId(this.context.getString(R.string.minterstitialad_ad_unit_id_main));
//        this.mInterstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdClosed() {
////                super.onAdClosed();
//                TitleActivity.this.requestNewInterstitial();
//                if (TitleActivity.this.flag == 3) {
//                    TitleActivity.this.flag = -1;
//                    TitleActivity.this.startActivity(TitleActivity.this.intentTitle);
//                }
//            }
//        });
//        requestNewInterstitial();
//    }
//
//    private void requestNewInterstitial() {
//        this.mInterstitialAd.loadAd(new AdRequest.Builder().build());
//    }

    private boolean isNetworkAvailable() {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_open_favourite) {
            Intent intent = new Intent(getApplicationContext(), TitleActivity.class);
            intent.putExtra("god", "favourite");
            intent.putExtra("actionBarTitle", "\u092e\u0947\u0930\u0947 \u092a\u0938\u0902\u0926\u0940\u0926\u093e (Favourite)");
            startActivity(intent);
            return true;
        } else if (id == R.id.action_share_app) {
            String msg = getResources().getString(R.string.share_msg);
            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.SEND");
            intent2.setType("text/plain");
            intent2.putExtra("android.intent.extra.TEXT", msg);
            startActivity(Intent.createChooser(intent2, "Share via"));
            return true;
        } else {
            if (id == R.id.action_rate_it) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + TitleActivity.this.getPackageName())));
            }

            else if (id == R.id.action_our_apps) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Digital_Learning")));
            }

            else if (id == R.id.action_disclaimer) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
                builder.setTitle((CharSequence) "Disclaimer");
                builder.setMessage(getResources().getString(R.string.discl));
                builder.setPositiveButton((CharSequence) "Ok", null);
                builder.setIcon((int) R.mipmap.ic_launcher);
                builder.show();
//                if (this.mInterstitialAd.isLoaded()) {
//                    this.mInterstitialAd.show();
//                } else {
//                    requestNewInterstitial();
//                }
            }
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

//        initAdmobCode();

        if (Utilities.checkInternet(this)) {
            adView.setVisibility(View.VISIBLE);
            adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
            adView.loadAd(adRequest);
            return;
        } else {
            adView.setVisibility(View.GONE);
            return;
        }

    }


    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }



    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }


    public static void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
