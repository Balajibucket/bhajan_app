package com.balaji_developer.bhaktiapp;


public class Model {

    int _id;
    String sDescription;
    String sTitle;
    String sType;
    int IsFavourite;

    public Model() {
    }


    public String getsType() {
        return sType;
    }

    public void setsType(String sType) {
        this.sType = sType;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getsDescription() {
        return sDescription;
    }

    public void setsDescription(String sDescription) {
        this.sDescription = sDescription;
    }

    public String getsTitle() {
        return sTitle;
    }

    public void setsTitle(String sTitle) {
        this.sTitle = sTitle;
    }

    public int getIsFavourite() {
        return IsFavourite;
    }

    public void setIsFavourite(int isFavourite) {
        IsFavourite = isFavourite;
    }






}
