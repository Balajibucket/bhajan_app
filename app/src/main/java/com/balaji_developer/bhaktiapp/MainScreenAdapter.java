package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class MainScreenAdapter extends BaseAdapter {
    private Context context;
    private final TypedArray menu_main_Icon;
    private final String[] menu_main_Item;

    public MainScreenAdapter(Context context) {
        this.context = context;
        this.menu_main_Item = context.getResources().getStringArray(R.array.bhajan_menu);
        this.menu_main_Icon = context.getResources().obtainTypedArray(R.array.bhajan_menu_icon);
    }

    public int getCount() {
        return this.menu_main_Item.length;
    }

    public String getItem(int position) {
        return this.menu_main_Item[position];
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.gridview_single_layout, null);
        }
        ((TextView) convertView.findViewById(R.id.grid_item_label)).setText(this.menu_main_Item[position]);
        ((ImageView) convertView.findViewById(R.id.grid_item_image)).setImageResource(this.menu_main_Icon.getResourceId(position, -1));
        return convertView;
    }
}
