package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdRequest.Builder;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

public class HomeActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    private StaggeredGridLayoutManager mLayoutManager;
    private Context context;
    //    Intent intentTitle;
//    int flag;
    public static InterstitialAd mInterstitialAd;
    private AdView adView;
    private AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_home1);

//        mAdView = (AdView) findViewById(R.id.adView);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle(getResources().getString(R.string.titleApp));
        this.context = getApplicationContext();
        getSupportActionBar().setIcon(ContextCompat.getDrawable(this.context, R.mipmap.ic_launcher));
        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);


        adView = (AdView) findViewById(R.id.adView);
        adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.minterstitialad_ad_unit_id_main));

        mInterstitialAd.loadAd(adRequest);

        RecyclerAdapter adapter = new RecyclerAdapter(this);
        mLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

    }


    @Override
    protected void onResume() {
        super.onResume();

//        initAdmobCode();

        if (Utilities.checkInternet(this)) {
            adView.setVisibility(View.VISIBLE);
            adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
            adView.loadAd(adRequest);
            return;
        } else {
            adView.setVisibility(View.GONE);
            return;
        }

    }


    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }


    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }


    public static void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }


//    public void initAdmobCode() {
//        MobileAds.initialize(getApplicationContext(), this.context.getString(R.string.app_id));
//
//        if (isNetworkAvailable()) {
//            mAdView.setVisibility(View.VISIBLE);
//            mAdView.loadAd(new Builder().build());
//        } else {
//            mAdView.setVisibility(View.GONE);
//        }
//        mInterstitialAd = new InterstitialAd(this);
//        mInterstitialAd.setAdUnitId(this.context.getString(R.string.minterstitialad_ad_unit_id_main));
//        mInterstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
////                super.onAdLoaded();
//
//                requestNewInterstitial();
//                if (HomeActivity.this.flag == 3) {
//                    HomeActivity.this.flag = -1;
////                    HomeActivity.this.startActivity(HomeActivity.this.intentTitle);
//                }
//            }
//        });
//        requestNewInterstitial();
//    }


//    public HomeActivity() {
//        this.flag = -1;
//    }

//    private void requestNewInterstitial() {
//        this.mInterstitialAd.loadAd(new Builder().build());
//    }

    private boolean isNetworkAvailable() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) getSystemService(context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_open_favourite) {
            Intent intent = new Intent(getApplicationContext(), TitleActivity.class);
            intent.putExtra("god", "favourite");
            intent.putExtra("actionBarTitle", "\u092e\u0947\u0930\u0947 \u092a\u0938\u0902\u0926\u0940\u0926\u093e (Favourite)");
            startActivity(intent);
            return true;
        } else if (id == R.id.action_share_app) {
            String msg = getResources().getString(R.string.share_msg);
            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.SEND");
            intent2.setType("text/plain");
            intent2.putExtra("android.intent.extra.TEXT", msg);
            startActivity(Intent.createChooser(intent2, "Share via"));
            return true;
        } else {
            if (id == R.id.action_rate_it) {
//                showInterstitial();
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + this.context.getPackageName())));
            } else if (id == R.id.action_our_apps) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Digital_Learning")));
            } else if (id == R.id.action_disclaimer) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
                builder.setTitle((CharSequence) "Disclaimer");
                builder.setMessage(getResources().getString(R.string.discl));
                builder.setPositiveButton((CharSequence) "Ok", null);
                builder.setIcon((int) R.mipmap.ic_launcher);
                builder.show();
//                if (this.mInterstitialAd.isLoaded()) {
//                    this.mInterstitialAd.show();
//                } else {
//                    requestNewInterstitial();
//                }
            } else if (id == android.R.id.home) {
                onBackPressed();
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
    }



}
