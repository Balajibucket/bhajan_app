package com.balaji_developer.bhaktiapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
//import android.support.v4.media.TransportMediator;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androidessence.pinchzoomtextview.PinchZoomTextView;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;


public class DescriptionActivity extends AppCompatActivity {
    private Context context;
    Item item;
    private PinchZoomTextView textViewTitleHindi;
//    private InterstitialAd mInterstitialAd;

    public static InterstitialAd mInterstitialAd;
    private AdView adView;
    private AdRequest adRequest;

    public DescriptionActivity() {
        this.item = new Item();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_description);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
//        getWindow().addFlags(TransportMediator.FLAG_KEY_MEDIA_NEXT);
        this.context = getApplicationContext();
        this.item = (Item) getIntent().getExtras().getSerializable("item");
        textViewTitleHindi = (PinchZoomTextView) findViewById(R.id.textViewShowDescription);
        textViewTitleHindi.setText(this.item.getsDescription());

        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/Roboto-Black.ttf");
        textViewTitleHindi.setTypeface(font);
        getSupportActionBar().setTitle(this.item.getsTitle());



        adView = (AdView) findViewById(R.id.adView);
        adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
        adView.loadAd(adRequest);

        mInterstitialAd = new InterstitialAd(this);
        // set the ad unit ID
        mInterstitialAd.setAdUnitId(getString(R.string.minterstitialad_ad_unit_id_main));

        mInterstitialAd.loadAd(adRequest);

    }

//    public void initAdmobCode() {
//        MobileAds.initialize(getApplicationContext(), this.context.getString(R.string.app_id));
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        if (isNetworkAvailable()) {
//            mAdView.setVisibility(View.VISIBLE);
//            mAdView.loadAd(new AdRequest.Builder().build());
//        } else {
//            mAdView.setVisibility(View.GONE);
//        }
//        this.mInterstitialAd = new InterstitialAd(this);
//        this.mInterstitialAd.setAdUnitId(this.context.getString(R.string.minterstitialad_ad_unit_id_main));
//        this.mInterstitialAd.setAdListener(new AdListener() {
//
//
//            @Override
//            public void onAdClosed() {
////                super.onAdClosed();
//                DescriptionActivity.this.requestNewInterstitial();
//            }
//        });
//        requestNewInterstitial();
//    }

//    private void requestNewInterstitial() {
//        this.mInterstitialAd.loadAd(new AdRequest.Builder().build());
//    }


    private boolean isNetworkAvailable() {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_details, menu);
        if (this.item.getbIsFavourite()) {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this.context, R.drawable.enable_star));
        } else {
            menu.getItem(0).setIcon(ContextCompat.getDrawable(this.context, R.drawable.ic_action_disable_star));
        }
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem i) {
        int id = i.getItemId();
        if (id == R.id.action_set_favourite) {
            if (this.item.getbIsFavourite()) {
                this.item.setbIsFavourite(false);
                i.setIcon(ContextCompat.getDrawable(this.context, R.drawable.ic_action_disable_star));
            } else {
                this.item.setbIsFavourite(true);
                i.setIcon(ContextCompat.getDrawable(this.context, R.drawable.enable_star));
            }


            if (new DataBaseHelper(this.context).saveFavourite(this.item)) {
                Toast.makeText(this.context, "Operation Performed Successfully", Toast.LENGTH_SHORT).show();
            }



//            if (this.mInterstitialAd.isLoaded()) {
//                this.mInterstitialAd.show();
//            } else {
//                requestNewInterstitial();
//            }
        } else if (id == R.id.action_share_content) {

            String PACKAGE_NAME = this.getPackageName();

            Intent intent2 = new Intent();
            intent2.setAction("android.intent.action.SEND");
            intent2.setType("text/plain");
            intent2.putExtra("android.intent.extra.TEXT", this.item.getsDescription() + "\n\nShared By : \u0906\u0930\u0924\u0940 \u092d\u091c\u0928 \u0914\u0930 \u091a\u093e\u0932\u0940\u0938\u093e App\nhttps://play.google.com/store/apps/details?id=" + PACKAGE_NAME);
            startActivity(Intent.createChooser(intent2, "Share via"));
        } else if (id == R.id.action_rate_it) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + DescriptionActivity.this.getPackageName())));
        } else if (id == R.id.action_our_apps) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://search?q=pub:Digital_Learning")));
        } else if (id == android.R.id.home) {

            onBackPressed();

        }

        return true;
    }

//    protected void onDestroy() {
//        super.onDestroy();
////        getWindow().clearFlags(TransportMediator.FLAG_KEY_MEDIA_NEXT);
//    }


    @Override
    protected void onResume() {
        super.onResume();

//        initAdmobCode();

        if (Utilities.checkInternet(this)) {
            adView.setVisibility(View.VISIBLE);
            adRequest = (new AdRequest.Builder()).setRequestAgent("android_studio:ad_template").build();
            adView.loadAd(adRequest);
            return;
        } else {
            adView.setVisibility(View.GONE);
            return;
        }

    }


    @Override
    public void onPause() {
        if (adView != null) {
            adView.pause();
        }
        super.onPause();
    }



    @Override
    public void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }


    public static void showInterstitial() {
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }

}
