package com.balaji_developer.bhaktiapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class DataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "balaji.db";

    private static Context myContext;

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.myContext = context;
    }

    public void onCreate(SQLiteDatabase db) {

       db.execSQL(
                "create table ItemTable " +
                        "(_id INTEGER PRIMARY KEY AUTOINCREMENT, sType text,sTitle text,sDescription text, IsFavourite INTEGER DEFAULT 0)"
        );

    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS ItemTable");
        onCreate(db);

    }

    public ArrayList<Item> getItemsList(String sType) {
        SQLiteDatabase db = this.getReadableDatabase();
        String sql;
        ArrayList<Item> itemArrayList = new ArrayList();
        Item item = new Item();
//        openDataBase();
        if (sType.equalsIgnoreCase("Favourite")) {
            sql = "select * from ItemTable where isFavourite > 0 ";
        } else {
            sql = "select * from ItemTable where sType like '" + sType + "'";
        }
        Cursor cursor = db.rawQuery(sql, null);
        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                item = new Item();
                item.setId(Integer.parseInt(cursor.getString(cursor.getColumnIndex("_id"))));
                item.setsType(cursor.getString(cursor.getColumnIndex("sType")));
                item.setsTitle(cursor.getString(cursor.getColumnIndex("sTitle")));
                item.setsDescription(cursor.getString(cursor.getColumnIndex("sDescription")));
                item.setbIsFavourite(cursor.getInt(cursor.getColumnIndex("IsFavourite")) > 0);
                itemArrayList.add(item);
                cursor.moveToNext();

            }
        }
        return itemArrayList;
    }

    public boolean saveFavourite(Item item) {
//        openDataBase();
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv
                = new ContentValues();
        cv.put("IsFavourite", Boolean.valueOf(item.getbIsFavourite()));
        return db.update("ItemTable", cv, new StringBuilder().append("_id=").append(item.getId()).toString(), null) > 0;
    }


    public boolean insertItem (Model user) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("_id", user.get_id());
        contentValues.put("sType", user.getsType());
        contentValues.put("sTitle", user.getsTitle());
        contentValues.put("sDescription", user.getsDescription());
        contentValues.put("IsFavourite", user.getIsFavourite());
//        long insertedid = db.insert("ItemTable", null, contentValues);

        long insertedid = db.insertWithOnConflict("ItemTable", null, contentValues,5);
        Log.e("insertedid","insertedid--->" + insertedid);
        return true;
    }

}
